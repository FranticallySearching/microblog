from flask import g
from flask_httpauth import HTTPBasicAuth, HTTPTokenAuth
from app.models import Member
from app.api.errors import error_response

basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth()


@basic_auth.verify_password
def verify_password(username, password):
    member = Member.query.filter_by(username=username).first()
    if member is None:
        return False
    g.current_user = member
    return member.check_password(password)


@basic_auth.error_handler
def basic_auth_error():
    return error_response(401)


@token_auth.verify_token
def verify_token(token):
    g.current_user = Member.check_token(token) if token else None
    return g.current_user is not None


@token_auth.error_handler
def token_auth_error():
    return error_response(401)
