from flask import g, jsonify, request, url_for

from app import db
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from app.models import Member


@bp.route('/members/<int:id>', methods=['GET'])
@token_auth.login_required
def get_member(id):
    return jsonify(Member.query.get_or_404(id).to_dict())


@bp.route('/members', methods=['GET'])
@token_auth.login_required
def get_members():
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Member.to_collection_dict(Member.query, page, per_page, 'api.get_members')
    return jsonify(data)


@bp.route('/members/<int:id>/followers', methods=['GET'])
@token_auth.login_required
def get_followers(id):
    member = Member.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Member.to_collection_dict(member.followers, page, per_page, 'api.get_followers', id=id)
    return jsonify(data)


@bp.route('/members/<int:id>/followed', methods=['GET'])
@token_auth.login_required
def get_followed(id):
    member = Member.query.get_or_404(id)
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = Member.to_collection_dict(member.followed, page, per_page, 'api.get_followed', id=id)
    return jsonify(data)


@bp.route('/members', methods=['POST'])
def create_member():
    data = request.get_json() or {}
    if 'username' not in data or 'email' not in data or 'password' not in data:
        return bad_request('must include username, email and password fields')
    if Member.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if Member.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    member = Member()
    member.from_dict(data, new_member=True)
    db.session.add(member)
    db.session.commit()
    response = jsonify(member.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_member', id=member.id)
    return response


@bp.route('/members/<int:id>', methods=['PUT'])
@token_auth.login_required
def update_member(id):
    member = Member.query.get_or_404(id)
    data = request.get_json() or {}
    if 'username' in data and data['username'] != member.username and \
            Member.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    if 'email' in data and data['email'] != member.email and \
            Member.query.filter_by(email=data['email']).first():
        return bad_request('please use a different email address')
    if g.current_user.get_token() != member.token:
        return bad_request('Attempting to edit a different member.')
    member.from_dict(data, new_member=False)
    db.session.commit()
    return jsonify(member.to_dict())
