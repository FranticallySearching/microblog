from flask import current_app, render_template
from flask_babel import _

from app.email import send_email


def send_password_reset_email(member):
    token = member.get_reset_password_token()
    send_email(_('[Microblog] Reset Your Password'),
               sender=current_app.config['ADMINS'][0],
               recipients=[member.email],
               text_body=render_template('email/reset_password.txt', member=member, token=token),
               html_body=render_template('email/reset_password.html', member=member, token=token))
