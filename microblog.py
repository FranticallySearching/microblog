from app import create_app, cli, db
from app.models import Member, Message, Notification, Post, Task

app = create_app()
cli.register(app)


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Member': Member, 'Post': Post, 'Message': Message, 'Notification': Notification, 'Task': Task}


# With cli imported, it will now show up in flask --help and flask translate --help commands
